<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Calculator</title>
    <link href="assets/css/style.css" rel="stylesheet">
    <script src="assets/js/script.js" defer></script>
</head>

<%--<form action="calculator" method="post">--%>
<div class="calculator-grid">
    <div class="output">
        <div data-previous-operand class="previous-operand"></div>
        <div data-current-operand class="current-operand"></div>
    </div>

    <button data-all-clear class="span-two">AC</button>
    <button type="button" data-delete>DEL</button>
    <button type="button" data-operation>/</button>
    <button type="button" data-number>7</button>
    <button type="button" data-number>8</button>
    <button type="button" data-number>9</button>
    <button type="button" data-operation>*</button>
    <button type="button" data-number>4</button>
    <button type="button" data-number>5</button>
    <button type="button" data-number>6</button>
    <button type="button" data-operation>+</button>
    <button type="button" data-number>1</button>
    <button type="button" data-number>2</button>
    <button type="button" data-number>3</button>
    <button type="button" data-operation>-</button>
    <button type="button" data-number>.</button>
    <button type="button" data-number>0</button>
    <button type="submit"data-equals class="span-two">=</button>
</div>
<%--</form>--%>
</body>
</html>